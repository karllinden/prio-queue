#!/bin/bash

args=()
for i in $(seq 1000)
do
    args+=($((RANDOM)))
done

out=$(mktemp --tmpdir "test.out.XXXXXXXX")
exp=$(mktemp --tmpdir "test.exp.XXXXXXXX")

./test "${args[@]}" > "$out"
printf '%s\n' "${args[@]}" | sort -V > "$exp"
diff -u "$exp" "$out"
status=$?

if [[ $status -eq 0 ]]
then
    echo "OK"
    rm "$out" "$exp"
else
    echo "NOT OK"
    echo "OUT: $out"
    echo "EXPECTED: $exp"
fi

exit $status
