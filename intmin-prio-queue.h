/*
 * This file is part of prio-queue.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef INTMIN_PRIO_QUEUE_H
# define INTMIN_PRIO_QUEUE_H

# include "prio-queue.h"

/**
 * Priority queue holding ints where the minimum int has highest
 * priority.
 */
struct intmin_prio_queue
{
    /**
     * The underlying priority queue.
     */
    struct prio_queue prio_queue;
};

/**
 * Initialize an empty intmin priority queue.
 *
 * @param impqp a pointer to the intmin priority queue
 * @return zero on success, or non-zero otherwise in which case errno is
 *         filled in appropriately
 */
int intmin_prio_queue_init(struct intmin_prio_queue * impqp);

/**
 * Deinitialize an intmin priority queue.
 *
 * @param impqp a pointer to the initialized intmin priority queueu
 */
void intmin_prio_queue_deinit(struct intmin_prio_queue * mpqp);

/**
 * Check if the intmin priority queue is empty.
 *
 * @param impqp a pointer to the intmin priority queue
 * @return non-zero if the priority queue is empty, or zero otherwise
 */
int intmin_prio_queue_is_empty(const struct intmin_prio_queue * impqp);

/**
 * Insert an integer into the priority queue.
 *
 * @param mpqp a pointer to the intmin priority queue
 * @param elem the element to insert
 * @return zero on success, or non-zero otherwise in which case errno is
 *         filled in appropriately
 */
int intmin_prio_queue_insert(struct intmin_prio_queue * impqp,
        int elem);

/**
 * Pull the least integer from the queue.
 *
 * @param impqp a pointer to the intmin priority queue
 * @return the least integer, or -1 if the priority queue was empty
 */
int intmin_prio_queue_pull(struct intmin_prio_queue * pqp);

/**
 * Return the least integer in the priority queue.
 *
 * @param impqp a pointer to the intmin priority queue
 * @return the minimum integer in the priority queue, or -1 if the
 *         priority queue is empty
 */
int intmin_prio_queue_peek(const struct intmin_prio_queue * impqp);

#endif /* INTMIN_PRIO_QUEUE_H */
