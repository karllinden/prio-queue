/*
 * This file is part of prio-queue.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <stdlib.h>
#include <string.h>

#include "prio-queue.h"

#define CAPACITY_STEP 10

int
prio_queue_is_empty(const struct prio_queue * pqp)
{
    return prio_queue_peek(pqp) == NULL;
}

static inline void *
pq_get(const struct prio_queue * pqp, size_t index)
{
    return (char *)pqp->heap + index * pqp->elemsize;
}

static inline int
pq_cmp(const struct prio_queue * pqp, const void * elemp1,
        const void * elemp2)
{
    return (*pqp->cmp_fn)(elemp1, elemp2);
}

static inline void
pq_move(const struct prio_queue * pqp, void * dest, const void * src)
{
    memcpy(dest, src, pqp->elemsize);
}

static inline void
pq_swap(const struct prio_queue * pqp, void * elemp1, void * elemp2)
{
    pq_move(pqp, pqp->tmp_elemp, elemp1);
    pq_move(pqp, elemp1, elemp2);
    pq_move(pqp, elemp2, pqp->tmp_elemp);
}

int
prio_queue_init(struct prio_queue * pqp, size_t elemsize,
        int (*cmp_fn)(const void *, const void *))
{
    pqp->cmp_fn = cmp_fn;
    pqp->elemsize = elemsize;
    pqp->size = 0;
    pqp->capacity = 0;
    pqp->heap = 0;

    pqp->tmp_elemp = malloc(elemsize);
    return pqp->tmp_elemp == NULL;
}

void
prio_queue_deinit(struct prio_queue * pqp)
{
    free(pqp->heap);
    free(pqp->tmp_elemp);
}

int
prio_queue_insert(struct prio_queue * pqp, const void * elemp)
{
    size_t index;
    void * this;
    void * next;

    /*
     * Allocate more space for the heap if necessary. The first index of
     * the heap is not used, whence +1 is needed on the capacity.
     */
    if (pqp->size == pqp->capacity) {
        size_t newcap = pqp->capacity + CAPACITY_STEP;
        void * new = realloc(pqp->heap, (newcap + 1) * pqp->elemsize);
        if (new == NULL) {
            return 1;
        }
        pqp->capacity = newcap;
        pqp->heap = new;
    }

    pqp->size++;

    index = pqp->size;
    this = pq_get(pqp, index);
    memcpy(this, elemp, pqp->elemsize);

    /*
     * Swap the element upwards in the heap as long as the parent (next)
     * has lower priority.
     */
    index >>= 1;
    next = pq_get(pqp, index);
    while (index > 0 && pq_cmp(pqp, next, this) < 0) {
        pq_swap(pqp, next, this);
        this = next;
        index >>= 1;
        next = pq_get(pqp, index);
    }

    return 0;
}

int
prio_queue_pull(struct prio_queue * pqp, void * elemp)
{
    void * parent;
    void * last;
    size_t parent_index = 1;
    size_t left_index = 2;
    size_t right_index = 3;

    if (pqp->size == 0) {
        return 1;
    }

    /*
     * Get the root element, the parent, of the heap and return it to
     * the caller.
     */
    parent = pq_get(pqp, parent_index);
    pq_move(pqp, elemp, parent);

    /*
     * Move the last element to the root of the heap. That element must
     * be swapped down the heap.
     */
    last = pq_get(pqp, pqp->size);
    pq_move(pqp, parent, last);
    pqp->size--;

    /*
     * Move the parent down the heap until the heap property is
     * fulfilled. Loop as long as there is a both a left and right
     * child.
     */
    while (right_index <= pqp->size) {
        void * left = pq_get(pqp, left_index);
        void * right = pq_get(pqp, right_index);
        void * max;

        /*
         * Get the child with maximum priority and update the parent
         * index so that the loop can continue. The maximum child is the
         * potential candidate for swapping up.
         */
        if (pq_cmp(pqp, left, right) > 0) {
            max = left;
            parent_index = left_index;
        } else {
            max = right;
            parent_index = right_index;
        }

        /*
         * Swap the maximum child up if it has higher priority than the
         * parent.
         */
        if (pq_cmp(pqp, max, parent) > 0) {
            pq_swap(pqp, max, parent);
        } else {
            return 0;
        }

        /* Update the indices for next iteration. */
        left_index = parent_index << 1;
        right_index = left_index + 1;
        parent = max;
    }

    /*
     * Although there is no right child there might still be a left
     * child. The left child cannot have any children. If there is a
     * left child and it has higher priority than the parent, then swap
     * it upwards.
     */
    if (left_index <= pqp->size) {
        void * left = pq_get(pqp, left_index);
        if (pq_cmp(pqp, left, parent) > 0) {
            pq_swap(pqp, left, parent);
        }
    }

    return 0;
}

const void *
prio_queue_peek(const struct prio_queue * pqp)
{
    return pqp->size != 0 ? pq_get(pqp, 1) : NULL;
}
