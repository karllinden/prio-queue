/*
 * This file is part of prio-queue.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

#include "intmin-prio-queue.h"

int
main(int argc, char * argv[])
{
    struct intmin_prio_queue impq;
    int exit_status = EXIT_FAILURE;
    int ret;
    int count = 0;
    int min = INT_MAX;
    int last = INT_MIN;

    ret = intmin_prio_queue_init(&impq);
    if (ret) {
        perror("intmin_prio_queue_init");
        return EXIT_FAILURE;
    }

    for (int i = 1; i < argc; ++ i) {
        int value = atoi(argv[i]);
        if (value < min) {
            min = value;
        }
        count++;

        ret = intmin_prio_queue_insert(&impq, value);
        if (ret) {
            perror("intmin_prio_queue_insert");
            goto error;
        }

        assert(!intmin_prio_queue_is_empty(&impq));
        assert(intmin_prio_queue_peek(&impq) == min);
    }

    while (!intmin_prio_queue_is_empty(&impq)) {
        int value = intmin_prio_queue_pull(&impq);
        printf("%d\n", value);

        count--;

        assert(value >= last);
        last = value;
    }

    assert(count == 0);

    exit_status = EXIT_SUCCESS;
error:
    intmin_prio_queue_deinit(&impq);
    return exit_status;
}
