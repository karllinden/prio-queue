/*
 * This file is part of prio-queue.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef PRIO_QUEUE_H
# define PRIO_QUEUE_H

# include <stddef.h>

/**
 * Polymorphic heap-based priority queue.
 */
struct prio_queue
{
    /**
     * A pointer to the compare function.
     */
    int (*cmp_fn)(const void * e1, const void * e2);

    /**
     * The size of the elements.
     */
    size_t elemsize;

    /**
     * The current size of the heap.
     */
    size_t size;

    /**
     * The capacity of the heap.
     */
    size_t capacity;

    /**
     * The heap.
     */
    void * heap;

    /**
     * Pointer to memory that can hold a temporary element.
     */
    void * tmp_elemp;
};

/**
 * Initialize an empty priority queue.
 *
 * @param pqp a pointer to the priority queue
 * @param elemsize the size of the elements of the priority queue
 * @param cmp_fn a pointer to the function to use for comparing
 *        priorities of the elements; the function should return an
 *        integer less than, equal to, or greater than zero if the first
 *        element is found, respectively, to have lower, equal, or
 *        greater priority than the second element
 * @return zero on success, or non-zero otherwise in which case errno is
 *         filled in appropriately
 */
int prio_queue_init(struct prio_queue * pqp, size_t elemsize,
        int (*cmp_fn)(const void *, const void *));

/**
 * Deinitialize a priority queue.
 *
 * @param pqp a pointer to the initialized priority queueu
 */
void prio_queue_deinit(struct prio_queue * pqp);

/**
 * Check if the priority queue is empty.
 *
 * @param pqp a pointer to the priority queue
 * @return non-zero if the priority queue is empty, or zero otherwise
 */
int prio_queue_is_empty(const struct prio_queue * pqp);

/**
 * Insert an element into the priority queue.
 *
 * The element will be copied into the priority queue.
 *
 * @param pqp a pointer to the priority queue
 * @param elemp the pointer to the element
 * @return zero on success, or non-zero otherwise in which case errno is
 *         filled in appropriately
 */
int prio_queue_insert(struct prio_queue * pqp, const void * elemp);

/**
 * Pull the element of highest priority from the queue.
 *
 * @param pqp a pointer to the priority queue
 * @param elemp a pointer to the destination of the returned element
 * @return zero on success, or non-zero if the priority queue was empty
 */
int prio_queue_pull(struct prio_queue * pqp, void * elemp);

/**
 * Return a pointer to the element with maximum priority in the queue.
 *
 * The pointer is only guaranteed to be valid as long as prio_queue_pull
 * or prio_queue_insrt is not called.
 *
 * @param pqp a pointer to the priority queue
 * @return a pointer to the element with maximum priority, or NULL if
 *         the queue is empty
 */
const void * prio_queue_peek(const struct prio_queue * pqp);

#endif /* PRIO_QUEUE_H */
