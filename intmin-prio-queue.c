/*
 * This file is part of prio-queue.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "intmin-prio-queue.h"

#define CAPACITY_STEP 10

static int
intmin_cmp(const void * cvp1, const void * cvp2)
{
    /* The least integer has highest priority. */
    const int * cip1 = cvp1;
    const int * cip2 = cvp2;
    return *cip2 - *cip1;
}

int
intmin_prio_queue_init(struct intmin_prio_queue * impqp)
{
    return prio_queue_init(&impqp->prio_queue, sizeof(int),
            &intmin_cmp);
}

void
intmin_prio_queue_deinit(struct intmin_prio_queue * impqp)
{
    prio_queue_deinit(&impqp->prio_queue);
}

int
intmin_prio_queue_is_empty(const struct intmin_prio_queue * impqp)
{
    return prio_queue_is_empty(&impqp->prio_queue);
}

int
intmin_prio_queue_insert(struct intmin_prio_queue * impqp, int elem)
{
    return prio_queue_insert(&impqp->prio_queue, &elem);
}

int
intmin_prio_queue_pull(struct intmin_prio_queue * impqp)
{
    int least;
    int ret;
    ret = prio_queue_pull(&impqp->prio_queue, &least);
    return ret == 0 ? least : -1;
}

int
intmin_prio_queue_peek(const struct intmin_prio_queue * impqp)
{
    const int * intp = prio_queue_peek(&impqp->prio_queue);
    return intp != NULL ? *intp : -1;
}
