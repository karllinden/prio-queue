#
#  This file is part of prio-queue.
#
#  Copyright 2018 Karl Linden <karl.j.linden@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

HEADERS = intmin-prio-queue.h prio-queue.h
OBJECTS = intmin-prio-queue.o prio-queue.o

all: test

clean:
	-rm -f $(OBJECTS) test

.PHONY: all clean

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -Wall -c -o $@ $<

test: test.c $(OBJECTS) $(HEADERS)
	$(CC) $(CFLAGS) -Wall -o $@ $< $(OBJECTS)
